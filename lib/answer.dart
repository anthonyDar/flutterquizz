import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  final Function answer;
  final String answerText;
  final score = 0;
  Answer(this.answer, this.answerText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        color: Colors.blue,
        textColor: Colors.white,
        onPressed: answer,
        child: Text(answerText),
      ),
    );
  }
}
