import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  String get resultPhrase {
    var resultText = 'you did it';
    if (resultScore <= 8) {
      resultText = 'You are awesome and innocent!';
    } else if (resultScore <= 12) {
      resultText = 'Pretty likeable!';
    } else if (resultScore <= 16) {
      resultText = 'You are ... Stange ?!';
    } else {
      resultText = 'You are so bad !!';
    }
    return resultText;
  }

  final Function resetQuizz;

  const Result(this.resultScore, this.resetQuizz);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Center(
          child: Text(
            resultPhrase,
            style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),
        FlatButton(
          onPressed: resetQuizz,
          child: Text('Restart Quizz !'),
          color: Colors.blue,
        )
      ],
    );
  }
}
